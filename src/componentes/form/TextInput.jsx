import React from "react";
import {Form, Label} from 'semantic-ui-react'

const TextInput = ({input,width,type,placeholder,meta:{touched,error}}) => {
    console.log("TextInput es :",{...input})
    return (
        <Form.Field error={touched && !error} type={type} with={width}>
            <input {...input} placeholder={placeholder}/>
            {
                touched && error && (
                    <Label basic color={"red"} pointing>
                        {error}
                    </Label>
                )
            }
        </Form.Field>
    )
}

export default TextInput
