import React from 'react'
import {Link} from 'react-router-dom'
import {Menu, Dropdown, DropdownMenu, Container, DropdownItem, Icon, Image, MenuItem} from 'semantic-ui-react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import {logout} from "../../app/store/actions/authActions";


const mapState = (state) => ({
    currentUser: state.auth.currentUser,
})

const actions = {
    logout,
}

const NavBar = ({currentUser,logout}) => {
    return (
        <Menu fixed='top' inverted>
            <Container>
                <MenuItem header as={Link} to={'/'}>
                    <Icon name='braille'/>Sistema
                </MenuItem>
                <MenuItem>
                    <Dropdown pointing='top left' text='Resource'>
                        <DropdownMenu>
                            <DropdownItem
                                text='Cursos'
                                icon='address card'
                                as={Link}
                                to={'cursos'}

                            />
                            <DropdownItem
                                text='Estudiantes'
                                icon='list ul'
                                as={Link}
                                to={'/estudiantes'}
                            />
                        </DropdownMenu>
                    </Dropdown>
                </MenuItem>
                <MenuItem>
                    <Dropdown pointing='top left' text='Matricula'>
                        <DropdownMenu>
                            <DropdownItem
                                text='Nueva Matricula '
                                icon='address card'
                                as={Link}
                                to={'/newMatricula'}

                            />
                            <DropdownItem
                                text='listar matriculas '
                                icon='list ul'
                                as={Link}
                                to={'/matriculas'}
                            />
                        </DropdownMenu>
                    </Dropdown>
                </MenuItem>
                <MenuItem position='right'>
                    <Image avatar spaced='right' src='/assets/user.png'></Image>
                    <Dropdown pointing='top left' text={currentUser.sub}>
                        <DropdownMenu>
                            <DropdownItem text='cerrar sesion '
                                          icon='log out'
                                          onClick={logout}
                            />
                        </DropdownMenu>
                    </Dropdown>
                </MenuItem>
            </Container>
        </Menu>
    )
}

NavBar.propTypes = {
    currentUser: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
}
export default connect(mapState,actions) (NavBar)

//rca
