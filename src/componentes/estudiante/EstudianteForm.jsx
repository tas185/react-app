import React, {useEffect, useState} from 'react'
import {combineValidators, isRequired} from 'revalidate'
import {Form as FinalForm, Field} from 'react-final-form'
import {Form, Header, Button} from 'semantic-ui-react'
import PropTypes from 'prop-types'

import useFetchEstudiante from "../../app/hooks/useFetchEstudiante";
import TextInput from '../form/TextInput'
import ErrorMessage from '../form/ErrorMessage'

const validate = combineValidators({
    nombre: isRequired({message: 'Por favor ingrese su nombre'}),
    apellido: isRequired({message: 'Por favor ingrese su apellido'}),
    dni: isRequired({message: 'Ingrese su DNI'}),
    edad: isRequired({message: 'La edad es un campo requerido'}),
    // edad: composeValidators(isRequired({ message: 'La edad es un campo requerido' }))(),
})

const EstudianteForm = ({estudianteId, submitHandler}) => {
    const [actionLabel, setActionLabel] = useState('Agregar Estudiante')

    //Custom hook
    // se estructura una variable segun los valores que retorn el hook
    //hook siempre se inicia al ciclo de vida del compoentne
    const [estudiante, loading] = useFetchEstudiante(estudianteId)

    // estudiente
    //useEffect : para guardar los valores del hook
    useEffect(() => {
        if (estudianteId) {
            setActionLabel('Editar Estudiante')
            console.log('Se va editar el Estudiante')
        } else setActionLabel('Agregar Estudiante')
    }, [estudianteId])

    return (

        //sumbmitHablder siempre retorna unos objetos
        //lo que pongas en el submit sera lo que se ejecutara en el handleSubmit
        <FinalForm
            onSubmit={(values) => submitHandler(values)}
            initialValues={estudianteId && estudiante}
            validate={validate}
            render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit}) => (
                <Form onSubmit={handleSubmit} error loading={loading}>
                    <Header as="h2" content={actionLabel} color="blue" textAlign="center"/>
                    <Field name="nombre" component={TextInput} placeholder="Ingrese su nombre"/>
                    <Field name="apellido" component={TextInput} placeholder="Ingrese su apellido"/>
                    <Field name="dni" component={TextInput} type="number" placeholder="Ingrese un numero de DNI"/>
                    <Field name="edad" component={TextInput} type="number" placeholder="Ingrese su edad"/>

                    {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid values"/>}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color="violet"
                        content={actionLabel}
                    />
                </Form>
            )}
        />
    )
}

EstudianteForm.propTypes = {
    estudianteId: PropTypes.string,
    submitHandler: PropTypes.func.isRequired,
}

EstudianteForm.defaultProps = {
    estudianteId: null
}

export default EstudianteForm
