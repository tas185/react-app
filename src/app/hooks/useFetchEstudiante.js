import { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import EstudianteService from "../base/estudianteService";

const useFetchEstudiante = (id) => {
    const [estudiante, setEstudiante] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        if (id) //esto es encadenamiento de promesas por eso no usa async await
          EstudianteService.fetchEstudiante(id)
                .then((response) => {
                    setEstudiante(response)
                })
                .catch((error) => {
                    toast.error(error)
                })
        setLoading(false)
    }, [id])

    //esto es lo que se devuelve
    return [estudiante, loading]
}

export default useFetchEstudiante
