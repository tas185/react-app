export const MODAL_OPEN = 'MODAL_OPEN'
export const  MODAL_CLOSE= 'MODAL_CLOSE'


//auth

export const LOGIN_USER= 'LOGIN_USER'
export const LOGOUT_USER= 'LOGOUT_USER'
export const CURRENT_USER= 'CURRENT_USER'

//curso
export const LOADING_CURSOS = 'LOADING_CURSOS'
export const LOADING_CURSO = 'LOADING_CURSO'
export const FETCH_CURSOS = 'FETCH_CURSOS'
export const FETCH_CURSO = 'FETCH_CURSO'
export const ADD_CURSO = 'ADD_CURSO'
export const UPDATE_CURSO = 'UPDATE_CURSO'
export const DELETE_CURSO = 'DELETE_CURSO'
