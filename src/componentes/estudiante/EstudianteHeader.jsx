import React from 'react'
import PropTypes from 'prop-types'
import { Segment, Grid, Item, Header, Icon } from 'semantic-ui-react'

const EstudianteHeader = ({ estudiante }) => {
    return (
        <Segment>
            <Grid>
                <Grid.Column width={12}>
                    <Item.Group>
                        <Item>
                            <Item.Content verticalAlign="middle">
                                <Header as="h1">
                                    {estudiante.nombre} {estudiante.apellido}
                                </Header>
                                <br />
                                {/*<Icon name="calendar alternate outline" />*/}
                                <Header as="h3">{estudiante.edad}</Header>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>
            </Grid>
        </Segment>
    )
}

EstudianteHeader.propTypes = {
    estudiante: PropTypes.object.isRequired,
}

export default EstudianteHeader
