import React, {useEffect, useState} from 'react'
import {Segment, Breadcrumb, Table, Divider, Header, Icon, Popup, Button, Container, Grid} from 'semantic-ui-react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import {toast} from 'react-toastify'
import {openModal, closeModal} from '../../app/store/actions/modalActions'
import LoadingComponent from '../../componentes/common/LoadingComponent'


import useFetchEstudiantes from "../../app/hooks/useFetchEstudiantes";
import EstudianteService from "../../app/base/estudianteService";
import EstudianteForm from "../../componentes/estudiante/EstudianteForm";

const actions = {
    openModal,
    closeModal,
}

const Estudiantes = ({openModal, closeModal}) => {
    const [estudiantesList, setEstudiantesList] = useState([])
    const [loadingAction, setLoadingAction] = useState(false)
    const [loading, setLoading] = useState(true)

    const [estudiantes] = useFetchEstudiantes()

    useEffect(() => {
        setLoading(true)
        if (estudiantes) {
            setEstudiantesList(estudiantes)
            setLoading(false)
        }
    }, [estudiantes])

    const handleCreateorEdit = async (values) => {

        const estudiantesUpdatedList = [...estudiantesList] //permite copiar el estado tal comose encuentra en ese momento
        try {

            if (values.id) {
                console.log(values)
                const updatedEstudiante = await EstudianteService.updateEstudiante(values)
                const index = estudiantesUpdatedList.findIndex((a) => a.id === values.id)
                estudiantesUpdatedList[index] = updatedEstudiante
                toast.info('El estudiante fue actualizado')
            } else {
                const estudiante = {
                    nombre: values.nombre,
                    apellido: values.apellido,
                    dni: values.dni,
                    edad: values.edad
                }
                const newEstudiante = await EstudianteService.addEstudiante(estudiante)
                estudiantesUpdatedList.push(newEstudiante)
                toast.success('El nuevo estudiante fue creado')
            }
            setEstudiantesList(estudiantesUpdatedList)
        } catch (error) {
            console.log("Error Estudiante:--->", error.message)

        }
        closeModal()
    }

    const handleDeleteEstudiante = async (id) => {
        setLoadingAction(true)
        try {
            let estudiantesUpdatedList = [...estudiantesList]
            await EstudianteService.deleteEstudiante(id)
            estudiantesUpdatedList = estudiantesUpdatedList.filter((a) => a.id !== id)
            setEstudiantesList(estudiantesUpdatedList)
            setLoadingAction(false)
            toast.info('El estudiante fue eliminado')
        } catch (error) {
            setLoadingAction(false)
            toast.error(error)
        }
    }

    let estudiantesArea = <h4>No existen categorias asociadas</h4>
    if (estudiantesList && estudiantesList.length > 0) {
        estudiantesArea = (
            // tamaño de grillas de 16
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="3">Nombre</Table.HeaderCell>
                        <Table.HeaderCell width="3">Apellido</Table.HeaderCell>
                        <Table.HeaderCell width="2">Documento</Table.HeaderCell>
                        <Table.HeaderCell width="2">Edad</Table.HeaderCell>
                        <Table.HeaderCell width="2"/>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {estudiantesList.map((estudiante) => (
                        <Table.Row key={estudiante.id}>
                            <Table.Cell>{estudiante.nombre}</Table.Cell>
                            <Table.Cell>{estudiante.apellido}</Table.Cell>
                            <Table.Cell>{estudiante.dni}</Table.Cell>
                            <Table.Cell>{estudiante.edad}</Table.Cell>
                            <Table.Cell>
                                <Popup
                                    inverted
                                    content="Actualiar Estudiante"
                                    trigger={
                                        <Button
                                            basic
                                            color="blue"
                                            icon="edit"
                                            loading={loadingAction}
                                            onClick={() => {
                                                openModal(<EstudianteForm estudianteId={estudiante.id}
                                                                          submitHandler={handleCreateorEdit}/>)
                                            }}
                                        />
                                        //se envia la referencia al metodo en submitHandle
                                    }
                                />
                                <Popup
                                    inverted
                                    content="Eliminar Estudiante"
                                    trigger={
                                        <Button

                                            basic

                                            color="red"
                                            icon="trash"
                                            loading={loadingAction}
                                            onClick={() => {
                                                handleDeleteEstudiante(estudiante.id)
                                            }}
                                        />
                                    }
                                />

                                {/*<Popup*/}
                                {/*    inverted*/}
                                {/*    content="Upload Photo"*/}
                                {/*    trigger={*/}
                                {/*        <Button*/}
                                {/*            color="vk"*/}
                                {/*            icon="cloud upload"*/}
                                {/*            loading={loadingAction}*/}
                                {/*            onClick={() => {*/}
                                {/*                openModal(<CustomerProfile customerId={customer.id} />, 'large', true)*/}
                                {/*            }}*/}
                                {/*        />*/}
                                {/*    }*/}
                                {/*/>*/}

                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        )
    }

    if (loading) return <LoadingComponent content="Cargando Estudiantes..."/>

    return (
        <>
            <Segment>
                <Breadcrumb size="small">
                    <Breadcrumb.Section>Resources</Breadcrumb.Section>
                    <Breadcrumb.Divider icon="right chevron"/>
                    <Breadcrumb.Section active>Estudiantes</Breadcrumb.Section>
                </Breadcrumb>
                <Divider horizontal>
                    <Header as="h4">
                        <Icon name="list alternate outline"/>
                        Estudiantes List
                    </Header>
                </Divider>
                <Segment>
                    <Button
                        size="large"
                        content="Nuevo Estudiante"
                        icon="add user"
                        color="green"
                        onClick={() => {
                            openModal(<EstudianteForm submitHandler={handleCreateorEdit}/>)
                        }}
                    />
                </Segment>
                <Container textAlign="center">
                    <Grid columns="3">
                        <Grid.Column width="3"/>
                        <Grid.Column width="10">{estudiantesArea}</Grid.Column>
                        <Grid.Column width="3"/>
                    </Grid>
                </Container>
            </Segment>
        </>
    )
}
//ancho del grid siempre es  16
Estudiantes.propTypes = {
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
}

export default connect(null, actions)(Estudiantes)
