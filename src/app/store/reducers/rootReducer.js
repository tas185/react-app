import {combineReducers} from 'redux'
import authReducer from "./authReducer";
import cursoReducer from './cursoReducer'
import modalReduce from "./modalReduce";

const rootReducer = combineReducers({

    modal: modalReduce,
    auth: authReducer,
    curso: cursoReducer,
})

export default rootReducer
