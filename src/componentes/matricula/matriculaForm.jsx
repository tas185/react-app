import React, { useEffect, useState } from 'react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, isRequired } from 'revalidate'
import { Button, Form, Grid, Header, Popup, Table } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import history from '../..'
import ErrorMessage from '../form/ErrorMessage'
// import TextAreaInput from '../form/TextAreaInput'
import SelectedInput from '../form/SelectInput'



import {fetchCursos} from "../../app/store/actions/CursoActions";
import useFetchEstudiantes from "../../app/hooks/useFetchEstudiantes";
import MatriculaService from "../../app/base/matriculaService";

const validate = combineValidators({
    estudiante: isRequired(''),
    curso: isRequired(''),
})

const actions = {
    fetchCursos,
}

const mapState = (state) => {
    const cursos = []

    if (state.curso.cursos && state.curso.cursos.length > 0) {
        state.curso.cursos.forEach((item) => {
            const curso = {
                key: item.id,
                text: item.nombre,
                value: item.id,
            }
            cursos.push(curso)
        })
    }
    return {
        loading: state.curso.loadingCursos,
        cursos,
    }
}

const MatriculaForm = ({ fetchCursos, cursos, loading }) => {
    const [estudiantes] = useFetchEstudiantes()
    const [estudiantesList, setEstudiantesList] = useState([])
    const [loadingEstudiantes, setLoadingEstudiantes] = useState(true)
    const [items, setItems] = useState([])
    const [item, setItem] = useState(null)

    useEffect(() => {
        if (cursos.length === 0) {
            fetchCursos()
        }
        setLoadingEstudiantes(true)
        if (estudiantes) {
            const estudiantesList = []
            estudiantes.forEach((item) => {
                const estudiante = {
                    key: item.id,
                    text: `${item.nombre} ${item.apellido}`,
                    value: item.id,
                }
                estudiantesList.push(estudiante)
            })
            setEstudiantesList(estudiantesList)
            setLoadingEstudiantes(false)
        }
    }, [estudiantes, cursos.length, fetchCursos])

    const handleAddingItems = () => {
        const newItems = [...items]
        const cursosList = [...cursos]
        const index = newItems.findIndex((a) => a.id === item)
        if (index > -1) {
            newItems[index] = {
                id: newItems[index].id,
                name: newItems[index].nombre,
                sigla: newItems[index].sigla,
                estado: newItems[index].estado,
            }
            setItems(newItems)
        } else {
            const newItem = {
                id: item,
                nombre: cursosList.filter((a) => a.key === item)[0].text,
            }
            newItems.push(newItem)
        }
        setItems(newItems)
    }

    const handleRemoveItems = (id) => {
        let updatedItems = [...items]
        updatedItems = updatedItems.filter((a) => a.id !== id)
        setItems(updatedItems)
    }

    const handleAddNewMatricula = async (values) => {
        const newItems = [...items]
        const itemsForMatricula = newItems.map((item) => {
            return {
                curso: {id: item.id }
            }
        })

        const newMatricula = {
            estudiante: {
                id: values.estudiante,
            },
            estado: true,
            items: itemsForMatricula,
        }
        try {
            const matricula = await MatriculaService.createMatricula(newMatricula)
            toast.info('La matricula fue creada')
            history.push(`matricula/${matricula.id}`)
        } catch (error) {
            console.log("handleAddNewMatricula ",error)
        }
    }

    return (
        <FinalForm
            onSubmit={(values) => handleAddNewMatricula(values)}
            validate={validate}
            render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
                <Form onSubmit={handleSubmit} error loading={loading || loadingEstudiantes}>
                    <Header as="h2" content="Formulario" color="blue" textAlign="center" />
                    <Field
                        name="estudiante"
                        component={SelectedInput}
                        placeholder="Elija un Estudiante"
                        options={estudiantesList}
                        width="3"
                    />
                    <Grid columns="2">
                        <Grid.Row columns="2">
                            <Grid.Column width="5">
                                <Field
                                    name="curso"
                                    component={SelectedInput}
                                    placeholder="Seleccione un curso."
                                    options={cursos}
                                    width="3"
                                    handleOnChange={(e) => setItem(e)}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <Popup
                                    inverted
                                    content="Agregar un curso a la matricula"
                                    trigger={
                                        <Button
                                            type="button"
                                            loading={submitting}
                                            color="blue"
                                            icon="plus circle"
                                            onClick={handleAddingItems}
                                            disabled={!item}
                                        />
                                    }
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            {items && items.length > 0 && (
                                <Table celled collapsing style={{ marginLeft: '2%' }}>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>Curso</Table.HeaderCell>
                                            {/*<Table.HeaderCell>Quantity</Table.HeaderCell>*/}
                                            <Table.HeaderCell />
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {items.map((item) => (
                                            <Table.Row key={item.id}>
                                                <Table.Cell>{item.nombre}</Table.Cell>
                                                {/*<Table.Cell textAlign="center">{item.quantity}</Table.Cell>*/}
                                                <Table.Cell>
                                                    <Popup
                                                        inverted
                                                        content="Eliminar de la matricula"
                                                        trigger={
                                                            <Button
                                                                color="red"
                                                                icon="remove circle"
                                                                type="button"
                                                                onClick={() => handleRemoveItems(item.id)}
                                                            />
                                                        }
                                                    />
                                                </Table.Cell>
                                            </Table.Row>
                                        ))}
                                    </Table.Body>
                                </Table>
                            )}
                        </Grid.Row>
                    </Grid>
                    <br />
                    {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalido valor" />}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine || items.length === 0}
                        loading={submitting}
                        color="green"
                        content="Agregar nueva "
                    />
                </Form>
            )}
        />
    )
}

MatriculaForm.propTypes = {
    fetchCursos: PropTypes.func.isRequired,
    cursos: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
}

export default connect(mapState, actions)(MatriculaForm)
