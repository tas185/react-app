import baseApi from "./baseApi";
import { MATRICULA_ENDPOINT} from "../core/appConstantes";

class MatriculaService {

    static fetchMatriculas = () => baseApi.get(MATRICULA_ENDPOINT+'/demo')

    static fetchMatricula = (id) => baseApi.get(`${MATRICULA_ENDPOINT}/${id}`)
    static createMatricula = (matricula) => baseApi.post(MATRICULA_ENDPOINT,matricula)
}

export default MatriculaService
