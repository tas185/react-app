import { useState, useEffect } from 'react'
// import { toast } from 'react-toastify'
import EstudianteService from "../base/estudianteService";

const useFetchEstudiantes = () => {
    const [estudiantes, setEstudiantes] = useState(null)
    const [loading, setLoading] = useState(false)   // se puede obiar

    useEffect(() => {
        setLoading(true)

        EstudianteService.fetchEstudiantes()
            .then((response) => {
                setEstudiantes(response)
            })
            .catch((error) => {
                // toast.error(error)
                console.log("Error useFetchEstudiantes",error)
            })
        setLoading(false)
    }, [])

    return [estudiantes, loading]
}

export default useFetchEstudiantes
