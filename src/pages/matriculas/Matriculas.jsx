import React, { useCallback, useEffect, useState } from 'react'
// import { toast } from 'react-toastify'
import { Breadcrumb, Button, Container, Divider, Grid, Header, Icon, Popup, Segment, Table } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import LoadingComponent from '../../componentes/common/LoadingComponent'
import MatriculaService from "../../app/base/matriculaService";

const Matriculas = ({ history }) => {
    const [matriculas, setMatriculas] = useState([])
    const [loading, setLoading] = useState(false)

    const fetchMatriculas = useCallback(async () => {
        setLoading(true)
        try {
            const matriculas = await MatriculaService.fetchMatriculas()
            if (matriculas){
                setMatriculas(matriculas)
            }
            setLoading(false)
        } catch (error) {
            setLoading(false)
            // toast.error(error)
            console.log("Error fetchMatriculas   :",error)
        }
    }, [])

    useEffect(() => {
        fetchMatriculas()
    }, [fetchMatriculas])

    let matriculasList = <h4 >Aqui no hay matriculas registradas</h4>

    console.log("MAtriculas",matriculas)
    if (matriculas && matriculas.length > 0) {
        matriculasList = (
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="3">Codigo</Table.HeaderCell>
                        <Table.HeaderCell width="3">Estudiante</Table.HeaderCell>
                        <Table.HeaderCell width="2">Estado</Table.HeaderCell>
                        <Table.HeaderCell width="2">Fecha</Table.HeaderCell>
                        <Table.HeaderCell width="2">Acciones</Table.HeaderCell>
                        <Table.HeaderCell width="3" />
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {matriculas.map((matricula) => (
                        <Table.Row key={matricula.id}>
                            <Table.Cell>{matricula.id}</Table.Cell>
                            <Table.Cell>{matricula.estudiante.nombre}</Table.Cell>
                            <Table.Cell>{matricula.estado ? 'Activo' : 'Inactivo'}</Table.Cell>
                            <Table.Cell>{new Date(matricula.fechaMatri).toLocaleDateString()}</Table.Cell>
                            <Table.Cell>
                                <Popup
                                    inverted
                                    content="Matricula Detalle"
                                    trigger={
                                        <Button
                                            basic
                                            color="green"
                                            icon="address card outline"
                                            onClick={() => {
                                                history.push(`/matricula/${matricula.id}`)
                                            }}
                                        />
                                    }
                                />
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        )
    }

    if (loading) return <LoadingComponent content="Loading Invoices..." />

    return (
        <Segment>
            <Breadcrumb size="small">
                <Breadcrumb.Section>Invoice</Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron" />
                <Breadcrumb.Section active>Matricula List</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as="h4">
                    <Icon name="list alternate outline" />
                    Matriculas
                </Header>
            </Divider>
            <Container>
                <Grid columns="3">
                    <Grid.Column width="3" />
                    <Grid.Column width="10">{matriculasList}</Grid.Column>
                    <Grid.Column width="3" />
                </Grid>
            </Container>
        </Segment>
    )
}

Matriculas.propTypes = {
    history: PropTypes.object.isRequired,
}

export default Matriculas
