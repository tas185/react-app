import React from 'react'

import { Breadcrumb, Container, Divider, Grid, Header, Icon, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import MatriculaForm from '../../componentes/matricula/matriculaForm'


const Matricula = () => {
    return (
        <Segment>
            <Breadcrumb size="small">
                <Breadcrumb.Section>Matricula</Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron" />
                <Breadcrumb.Section as={Link} to="/matricula">
                    Matricula List
                </Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron" />
                <Breadcrumb.Section active>Nueva MatriculaNew</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as="h4">
                    <Icon name="address card outline" />
                    matricula Detalle
                </Header>
            </Divider>
            <Container>
                <Grid columns="3">
                    <Grid.Column width="3" />
                    <Grid.Column width="10">
                        <MatriculaForm />
                    </Grid.Column>
                    <Grid.Column width="3" />
                </Grid>
            </Container>
        </Segment>
    )
}

export default Matricula
