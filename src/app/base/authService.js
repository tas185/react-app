import baseApi from "./baseApi";
import {AUTH_ENDPOINT} from '../core/appConstantes'
import {setToken,removeToken,getDecodedToken} from '../config/auth/credentials'

class AuthService {
    static login =async (credentials) => {
        try
        {
            // console.log("login Service AUTH_ENDPOINT:", AUTH_ENDPOINT , credentials)

             const response = await baseApi.post(AUTH_ENDPOINT,credentials)

            if (response)setToken(response.token)
        }catch (er)
        {
            // console.log("fallo en ********:",er)
            throw  er
        }
    }
    static currentUser =() => getDecodedToken()

    static logout = () => removeToken()
}

export default AuthService


