import {CURSOS_ENDPOINT} from "../core/appConstantes";
import baseApi from "./baseApi";

const getCursoUrl =(id)=> `${CURSOS_ENDPOINT}/${id}`

class CursoService {

    static fetchCursos= () => baseApi.get(CURSOS_ENDPOINT)

    static fetchCurso =async (id) => baseApi.get(getCursoUrl(id))

    static  addCurso =async (curso)=> baseApi.post(CURSOS_ENDPOINT,curso)

    static updateCurso =async (curso) => baseApi.put(CURSOS_ENDPOINT,curso)

    static removeCurso = async (id) => baseApi.delete(getCursoUrl(id))
}

export default CursoService
