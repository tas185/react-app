import React, {useEffect, useState} from "react";
import { Form, Header, Button } from 'semantic-ui-react'
import { Form as FinalForm, Field } from 'react-final-form'
import {combineValidators, isRequired} from "revalidate";


import {connect }from 'react-redux';
import PropTypes from 'prop-types'
import ErrorMessage from "../form/ErrorMessage";
import TextInput from "../form/TextInput";
// import ControlSelect from "../form/ControlSelect";

import {fetchCurso,addCurso,updateCurso} from "../../app/store/actions/CursoActions";
import SelectedInput from "../form/SelectInput";
// import LoadingComponent from "../common/LoadingComponent";


const  validate = combineValidators(
    {
        // nombre: isRequired({message: 'El nombre es requerido'}),
        // sigla: isRequired({message: 'La sigla es requerida'}),
        estado: isRequired({message:'Es estado es requerido'}),
    }
)

const actions = {
    fetchCurso,
    addCurso,
    updateCurso,
}

const mapState = (state) => ({
    curso: state.curso.curso,
    loading: state.curso.loadingCurso,
})


// const CursosForm =({ id, curso, fetchCurso,loading,addCurso,updateCurso,estado}) => {
const CursosForm =({ id, curso, fetchCurso,loading,addCurso,updateCurso,estado}) => {

    const [actionLabel,SetActionLabel] = useState('Agregar Curso')
    //hook
    // const [valor,setValor]= useState(estado)
    const [item, setItem] = useState(estado)
    const options = [
        {key:'activo' ,text: 'Activo',value:true},
        {key:'inactivo' ,text: 'Inactivo',value:false},
    ]


     useEffect(()=>  {
        if (id)
        {
            fetchCurso(id)
            // if(curso)
            //     curso.estado=valor
            SetActionLabel('Editar Curso')
        }else {
            SetActionLabel('Agregar Curso')
        }

    },[fetchCurso,id])

    const handleCreateorEdit = (values) => {
        // console.log("se va cambiar",values)
        if (id) {

            //console.log("values: ",values)
            updateCurso(values)
        } else {
            const newCurso = {
                nombre: values.nombre,
                sigla: values.sigla,
                estado: item,
            }
            addCurso(newCurso)
        }
    }

    //if (!curso) return <LoadingComponent content="Cargando formulario..." />
    //if (!curso) curso: {curso.estado=false}
    // console.log("dato de CursosForm:*********************",curso)

    return (
        <FinalForm
            onSubmit={(values) => handleCreateorEdit(values)}
            initialValues={id && curso}
            validate={validate}
            render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
                <Form onSubmit={handleSubmit} error loading={loading}>
                    <Header as="h2" content={actionLabel} color="blue" textAlign="center" />
                    <Field name="nombre" component={TextInput} placeholder="Ingrese nombre" />
                    <Field name="sigla" component={TextInput} placeholder="Ingrese la sigla" />
                    <Field
                        name="estado"
                        component={SelectedInput}
                        placeholder="Seleccionar"
                        options={options}
                        width="6"
                        handleOnChange={(e) => setItem(e)}
                    />
                    {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid values"  />}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color="blue"
                        content={actionLabel}
                    />
                </Form>
            )}
        />
    )
}


CursosForm.prototype= {
    id: PropTypes.string,
    curso: PropTypes.object,
    fetchCurso: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    addCurso: PropTypes.func.isRequired,
    updateCurso: PropTypes.func.isRequired,
    handlePadre: PropTypes.func.isRequired,
}


CursosForm.defaultProps = {
    id: null,
    curso: null,
    // estado: false,
}
export default connect(mapState, actions)(CursosForm)
