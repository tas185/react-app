import jwtDecode from 'jwt-decode'
import {TOKEN_KEY} from '../../core/appConstantes'

export const setToken = (token) => {
    if (token) localStorage.setItem(TOKEN_KEY,token)
}


export const getJwt = () => localStorage.getItem(TOKEN_KEY)

export const getDecodedToken = () => {
    try{
        return jwtDecode(getJwt())
    }catch (er){
        console.log("Error getJwt",er)
        return null
    }
}

export const removeToken = () => {
    localStorage.removeItem(TOKEN_KEY)
}
