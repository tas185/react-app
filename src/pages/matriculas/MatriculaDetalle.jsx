import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
// import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import LoadingComponent from "../../componentes/common/LoadingComponent";
import MatriculaService from "../../app/base/matriculaService";
import EstudianteService from "../../app/base/estudianteService";
import CursoService from "../../app/base/cursoService";
import { Breadcrumb, Container, Divider, Grid, Header, Icon, Label, Segment, Table } from 'semantic-ui-react'



const MatriculaDetalle = ({match}) => {

    const [matricula,setMatricula] = useState(null)
    const [loading,setLoading] = useState(false)

    const fetchMatricula = useCallback(async ()=> {
        setLoading(true)
        try
        {
            const matricula = await MatriculaService.fetchMatricula(match.params.id)
            if (matricula){
                const estudiante = await EstudianteService.fetchEstudiante(matricula.estudiante.id)
                const items =[]

                if (matricula.items.length>0) {

                    matricula.items.forEach((item) => {
                        CursoService.fetchCurso(item.curso.id).then((response) => {
                            if (response){
                                const cursoItem = {
                                    id: response.id,
                                    sigla: response.sigla,
                                    nombre: response.nombre,
                                }

                                items.push(cursoItem)
                            }

                            // const totalCantidad= items.reduce((sum,{n})=> sum + matricula.items.length,0)
                            //matricula.items.length


                            const matriculaDetalle = {
                                id: matricula.id,
                                estado: matricula.estado,
                                estudiante,
                                items,
                                createdAt: new Date(matricula.fechaMatri).toLocaleDateString(),
                                // totalCantidad
                            }
                            setMatricula(matriculaDetalle)

                        })
                    })

                }
            }
            setLoading(false)
        }catch (er){
            setLoading(false)
            // toast.error(er)
            console.log("Matricula Detalle:",er)
        }
    },[match.params.id])


    useEffect(()=> {
        fetchMatricula()
    },[fetchMatricula])

    if (loading) return <LoadingComponent content="Loading Matricula Details..." />
    let matriculaDetailedArea = <h4>Matricula Detalle</h4>


    if (matricula) {
        matriculaDetailedArea = (
            <Segment.Group>
                <Segment>
                    <Header as="h4" block color="violet">
                        Estudiante
                    </Header>
                </Segment>
                <Segment.Group>
                    <Segment>
                        <p>
                            <strong>Nombre: </strong>
                            {`${matricula.estudiante.nombre} ${matricula.estudiante.apellido}`}
                        </p>
                    </Segment>
                </Segment.Group>
                <Segment>
                    <Header as="h4" block color="blue">
                        Matricula
                    </Header>
                </Segment>
                <Segment.Group>
                    <Segment>
                        <p>
                            <strong>Matricula Code: </strong>
                            {matricula.id}
                        </p>
                        <p>
                            <strong>Fecha: </strong>
                            {matricula.createdAt}
                        </p>
                        <p>
                            <strong>Estado: </strong>
                            {matricula.estado? 'Activo': 'Inactivo'}
                        </p>
                        {/*<p>*/}
                        {/*    <strong>Created At: </strong>*/}
                        {/*    {invoice.createdAt}*/}
                        {/*</p>*/}
                    </Segment>
                </Segment.Group>
                <Segment>
                    <Table celled striped color="violet">
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan="4">
                                    <Icon name="braille" />
                                    Cursos
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Nombre</Table.HeaderCell>
                                <Table.HeaderCell>sigla</Table.HeaderCell>
                                <Table.HeaderCell>estado</Table.HeaderCell>
                                {/*<Table.HeaderCell>Total (/S)</Table.HeaderCell>*/}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {matricula.items.length > 0 &&
                            matricula.items.map((item) => (
                                <Table.Row key={item.id}>
                                    <Table.Cell>{item.nombre}</Table.Cell>
                                    <Table.Cell>{item.sigla}</Table.Cell>
                                    <Table.Cell>{item.estado? 'Activo' : 'Inactivo'}</Table.Cell>
                                </Table.Row>
                            ))}
                        </Table.Body>
                    </Table>
                    <Container textAlign="right">
                        <Label basic color="green" size="large">
                            Total Cursos Matriculados:
                            <Label.Detail content={`Total Cursos: ${matricula.items.length}`} />
                        </Label>
                    </Container>
                </Segment>
            </Segment.Group>
        )
    }

    return (
        <Segment>
            <Breadcrumb size="small">
                <Breadcrumb.Section>Matricula</Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron" />
                <Breadcrumb.Section as={Link} to="/matriculas">
                    Matricula List
                </Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron" />
                <Breadcrumb.Section active>Matricula Detalle</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as="h4">
                    <Icon name="address card outline" />
                    Matricula Detail
                </Header>
            </Divider>
            <Container>
                <Grid columns="3">
                    <Grid.Column width="3" />
                    <Grid.Column width="10">{matriculaDetailedArea}</Grid.Column>
                    <Grid.Column width="3" />
                </Grid>
            </Container>
        </Segment>
    )
}

MatriculaDetalle.propTypes = {
    match: PropTypes.object.isRequired,
}

export default MatriculaDetalle
