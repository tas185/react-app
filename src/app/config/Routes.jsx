import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { Container } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import NotFound from '../../componentes/common/NotFound'
import Navbar from '../../componentes/navbar/NavBar'
import HomePage from '../../pages/home/HomePage'


import Cursos from '../../pages/cursos/Cursos'
import Estudiantes from '../../pages/estudiantes/Estudiantes'
import Matricula from '../../pages/matriculas/Matricula'
import Matriculas from '../../pages/matriculas/Matriculas'
import MatriculasDetalle from '../../pages/matriculas/MatriculaDetalle'


const Routes = ({ authenticated }) => {
    return (
        <>
            <Route exact path="/" component={HomePage} />
            {authenticated && (
                <Route
                    path="/(.+)"
                    render={() => (
                        <>
                            <Navbar />
                            <Container style={{ marginTop: '7em' }}>
                                <Switch>
                                    <Route exact path="/" component={HomePage} />
                                    <Route path="/estudiantes" component={Estudiantes} />
                                    <Route path="/cursos" component={Cursos} />
                                    <Route path="/newMatricula" component={Matricula} />
                                    <Route path="/matricula/:id" component={MatriculasDetalle} />
                                    <Route path="/matriculas" component={Matriculas} />
                                    <Route component={NotFound} />
                                </Switch>
                            </Container>
                        </>
                    )}
                />
            )}
        </>
    )
}

Routes.propTypes = {
    authenticated: PropTypes.bool,
}

Routes.defaultProps = {
    authenticated: false,
}

export default withRouter(Routes)
