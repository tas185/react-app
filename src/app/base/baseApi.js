import axios from 'axios'
import {toast} from "react-toastify";
import {getJwt} from "../config/auth/credentials";
import {TOKEN_KEY} from "../core/appConstantes";
import history from '../../'
const http = axios.create({
    baseURL: "http://localhost:8080",
})


http.interceptors.response.use(undefined, (error) => {
        if (error.message === 'Network Error' &&
            !error.response) {
            toast.error('Network error - make sure the API server is runing')
            window.localStorage.removeItem(TOKEN_KEY)
            history.push('/')
            toast.info('Tu sesion ha expirado, porfavor ingresa de nuevo.')
        }
        const {status, data, config} = error.response

        if (status === 404) {
            history.push('/notFound')
        }

        if (status === 400 &&
            config.method === 'get' && data.error.hasOwnProperty('id')) {
            history.push('/notFound')
        }

        if (status === 500) {
            toast.error('Server error - revisar el terminal para mas informacion !')
        }
    console.log("Error response:",error.response)
    throw error.response
})


//agregamos el token
http.interceptors.request.use(
    (config) => {
        const token = getJwt()
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
    },
    (error) => Promise.reject(error)
)

const responseBody = (response) => response.data

const baseApi = {
    get: (url) => http.get(url).then(responseBody),
    post: (url, body) => http.post(url, body).then(responseBody),
    put: (url, body) => http.put(url, body).then(responseBody),
    delete: (url) => http.delete(url).then(responseBody),
}

export default baseApi
