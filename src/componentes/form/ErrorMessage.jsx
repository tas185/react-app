import React from 'react'
import PropTypes from 'prop-types'
import {MessageHeader ,Message ,MessageList ,MessageItem }from 'semantic-ui-react'

const ErrorMessage = ({ error, text }) => {
    return (
        <Message error>
            <MessageHeader>{error.statusText}</MessageHeader>
            {error.data && Object.keys(error.data.errors).length > 0 && (
                <MessageList>
                    {Object.values(error.data.errors)
                        .flat()
                        .map((err, i) => (
                            // eslint-disable-next-line react/no-array-index-key
                            <MessageItem key={i}>{err}</MessageItem>
                        ))}
                </MessageList>
            )}
            {text && <Message.Content content={text} />}
        </Message>
    )
}

ErrorMessage.propTypes = {
    error: PropTypes.any.isRequired,
    text: PropTypes.string.isRequired,
}

export default ErrorMessage
